# Present demo

Download present by 

`$ go install golang.org/x/tools/cmd/present@latest`

then run

`$ ./run.sh`

it will download Dataddo theme from

`gitlab.com/prochac.dataddo/present-dataddo-theme`

and run `present` tool.