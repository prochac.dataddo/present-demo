#!/bin/sh

# init submodules
git submodule update --init --recursive
# and run present
present \
  -base=dataddo-theme \
  -use_playground \
  -content=. \
  -http=127.0.0.1:3999 \
  -orighost=localhost \
  -notes \
  -play
